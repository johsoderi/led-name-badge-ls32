#!/usr/bin/env python3
import sys

res = ""
for i in ["space","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","Å","Ä","Ö","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","å","ä","ö","0","1","2","3","4","5","6","7","8","9","@","tilde","question","plus","minus","equals","slash","doubleq","single","and","under","lpar","lpar2","hakr","parr","hakl","parl","comma","i2","dollar","proc2","semi3","com2","q12","upp","backtick","tick","gt","lt","reg","copy","ast","æ","AE","Ø"]:
    with open ("az/"+i+".txt", "r") as f:
        lines = f.readlines()
    for line in lines:
        byte_int = int(line.rstrip(), 2)
        hex_str = str(hex(byte_int))
        if (len(hex_str) == 3):
            hex_str = "0x0" + hex_str[2]
        res += hex_str + ", "
    res += " # " + str(i) + "\n"
print(res)
