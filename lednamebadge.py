#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
#
# (C) 2019 juergen@fabmail.org
#
# This is an upload tool for e.g.
# https://www.sertronics-shop.de/computer/pc-peripheriegeraete/usb-gadgets/led-name-tag-11x44-pixel-usb
# The font_11x44[] data was downloaded from such a device.
#
# Ubuntu install:
# ---------------
#  sudo apt-get install python3-usb
#
# Optional for image support:
#  sudo apt-get install python3-pil
#
# Windows install:
# ----------------
##    https://sourceforge.net/projects/libusb-win32/ ->
##      -> https://kent.dl.sourceforge.net/project/libusb-win32/libusb-win32-releases/1.2.6.0/libusb-win32-bin-1.2.6.0.zip
##      cd libusb-win32-bin-1.2.6.0\bin
## download inf-wizard.exe to your desktop. Right click 'Run as Administrator'
#       -> Click 0x0416 0x5020 LS32 Custm HID
#       -> Next -> Next -> Dokumente LS32_Sustm_HID.inf -> Save
#       -> Install Now... -> Driver Install Complete -> OK
# download python from python.org
#      [x] install Launcher for all Users
#      [x] Add Python 3.7 to PATH
#       -> Click the 'Install Now ...' text message.
#       -> Optionally click on the 'Disable path length limit' text message. This is always a good thing to do.
# run cmd.exe as Administrator, enter:
#    pip install pyusb
#    pip install pillow
#

#
# v0.1, 2019-03-05, jw  initial draught. HID code is much simpler than expected.
# v0.2, 2019-03-07, jw  support for loading bitmaps added.
# v0.3              jw  option -p to preload graphics for inline use in text.
# v0.4, 2019-03-08, jw  Warning about unused images added. Examples added to the README.
# v0.5,             jw  Deprecated -p and CTRL-characters. We now use embedding within colons(:)
#                       Added builtin icons and -l to list them.
# v0.6, 2019-03-14, jw  Added --mode-help with hints and example for making animations.
#                       Options -b --blink, -a --ants added. Removed -p from usage.
# v0.7, 2019-05-20, jw  Support pyhidapi, fallback to usb.core. Added python2 compatibility.
# v0.8, 2019-05-23, jw  Support usb.core on windows via libusb-win32
# v0.9, 2019-07-17, jw  Support 48x12 configuration too.
# v0.10, 2019-09-09, jw Support for loading monochrome images. Typos fixed.
# v0.11, 2019-09-29, jw New option --brightness added.
# v0.12, 2019-12-27, jw hint at pip3 -- as discussed in https://github.com/jnweiger/led-name-badge-ls32/issues/19
# v0.13, 2023-11-14, bs modularization.
#     Some comments about this big change:
#     * I wanted to keep this one-python-file-for-complete-command-line-usage, but also needed to introduce importable
#       classes for writing own content to the device (my upcoming GUI editor). Therefore, the file was renamed to an
#       importable python file, and forwarding python files are introduced with the old file names for full
#       compatibility.
#     * A bit of code rearranging and cleanup was necessary for that goal, but I hope the original parts are still
#       recognizable, as I tried to keep all this refactoring as less, as possible and sense-making, but did not do
#       the full clean-codish refactoring. Keeping the classes in one file is part of that refactoring-omittance.
#     * There is some initialization code executed in the classes not needed, if not imported. This is nagging me
#       somehow, but it is acceptable, as we do not need to save every processor cycle, here :)
#     * Have fun!


import argparse
import os
import re
import sys
import time
from array import array
from datetime import datetime
import importlib

__version = "0.13"


class SimpleTextAndIcons:
    bitmap_named = {
        'ball': (array('B', (
            0b00000000,
            0b00000000,
            0b00111100,
            0b01111110,
            0b11111111,
            0b11111111,
            0b11111111,
            0b11111111,
            0b01111110,
            0b00111100,
            0b00000000
        )), 1, '\x1e'),
        'happy': (array('B', (
            0b00000000,  # 0x00
            0b00000000,  # 0x00
            0b00111100,  # 0x3c
            0b01000010,  # 0x42
            0b10100101,  # 0xa5
            0b10000001,  # 0x81
            0b10100101,  # 0xa5
            0b10011001,  # 0x99
            0b01000010,  # 0x42
            0b00111100,  # 0x3c
            0b00000000  # 0x00
        )), 1, '\x1d'),
        'happy2': (array('B', (0x00, 0x08, 0x14, 0x08, 0x01, 0x00, 0x00, 0x61, 0x30, 0x1c, 0x07,
                               0x00, 0x20, 0x50, 0x20, 0x00, 0x80, 0x80, 0x86, 0x0c, 0x38, 0xe0)), 2, '\x1c'),
        'heart': (array('B', (0x00, 0x00, 0x6c, 0x92, 0x82, 0x82, 0x44, 0x28, 0x10, 0x00, 0x00)), 1, '\x1b'),
        'HEART': (array('B', (0x00, 0x00, 0x6c, 0xfe, 0xfe, 0xfe, 0x7c, 0x38, 0x10, 0x00, 0x00)), 1, '\x1a'),
        'heart2': (array('B', (0x00, 0x0c, 0x12, 0x21, 0x20, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01,
                               0x00, 0x60, 0x90, 0x08, 0x08, 0x08, 0x10, 0x20, 0x40, 0x80, 0x00)), 2, '\x19'),
        'HEART2': (array('B', (0x00, 0x0c, 0x1e, 0x3f, 0x3f, 0x3f, 0x1f, 0x0f, 0x07, 0x03, 0x01,
                               0x00, 0x60, 0xf0, 0xf8, 0xf8, 0xf8, 0xf0, 0xe0, 0xc0, 0x80, 0x00)), 2, '\x18'),
        'fablab': (array('B', (0x07, 0x0e, 0x1b, 0x03, 0x21, 0x2c, 0x2e, 0x26, 0x14, 0x1c, 0x06,
                               0x80, 0x60, 0x30, 0x80, 0x88, 0x38, 0xe8, 0xc8, 0x10, 0x30, 0xc0)), 2, '\x17'),
        'bicycle': (array('B', (0x01, 0x02, 0x00, 0x01, 0x07, 0x09, 0x12, 0x12, 0x10, 0x08, 0x07,
                                0x00, 0x87, 0x81, 0x5f, 0x22, 0x94, 0x49, 0x5f, 0x49, 0x80, 0x00,
                                0x00, 0x80, 0x00, 0x80, 0x70, 0xc8, 0x24, 0xe4, 0x04, 0x88, 0x70)), 3, '\x16'),
        'bicycle_r': (array('B', (0x00, 0x00, 0x00, 0x00, 0x07, 0x09, 0x12, 0x13, 0x10, 0x08, 0x07,
                                  0x00, 0xf0, 0x40, 0xfd, 0x22, 0x94, 0x49, 0xfd, 0x49, 0x80, 0x00,
                                  0x40, 0xa0, 0x80, 0x40, 0x70, 0xc8, 0x24, 0x24, 0x04, 0x88, 0x70)), 3, '\x15'),
        'owncloud': (array('B', (0x00, 0x01, 0x02, 0x03, 0x06, 0x0c, 0x1a, 0x13, 0x11, 0x19, 0x0f,
                                 0x78, 0xcc, 0x87, 0xfc, 0x42, 0x81, 0x81, 0x81, 0x81, 0x43, 0xbd,
                                 0x00, 0x00, 0x00, 0x80, 0x80, 0xe0, 0x30, 0x10, 0x28, 0x28, 0xd0)), 3, '\x14'),
    }

    bitmap_builtin = {}
    for i in bitmap_named:
        bitmap_builtin[bitmap_named[i][2]] = bitmap_named[i]


    def __init__(self, font=getattr(importlib.import_module("fonts.default"), 'Font')):
        self.font = font
        self.char_offsets = {}
        for i in range(len(self.font.charmap)):
            self.char_offsets[self.font.charmap[i]] = 11 * i
            #print(i, self.font.charmap[i], self.char_offsets[self.font.charmap[i]])
        self.bitmap_preloaded = [([], 0)]
        self.bitmaps_preloaded_unused = False

    def add_preload_img(self, filename):
        """Still used by main, but deprecated. PLease use ":"-notation for bitmap() / bitmap_text()"""
        self.bitmap_preloaded.append(SimpleTextAndIcons.bitmap_img(filename))
        self.bitmaps_preloaded_unused = True


    def are_preloaded_unused(self):
        """Still used by main, but deprecated. PLease use ":"-notation for bitmap() / bitmap_text()"""
        return self.bitmaps_preloaded_unused == True


    @staticmethod
    def _get_named_bitmaps_keys():
        return SimpleTextAndIcons.bitmap_named.keys()


    def bitmap_char(self, ch):
        """Returns a tuple of 11 bytes, it is the bitmap data of given character.
            Example: ch = '_' returns (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 255).
            The bits in each byte are horizontal, highest bit is left.
        """
        if ord(ch) < 32:
            if ch in SimpleTextAndIcons.bitmap_builtin:
                return SimpleTextAndIcons.bitmap_builtin[ch][:2]

            self.bitmaps_preloaded_unused = False
            return self.bitmap_preloaded[ord(ch)]

        o = self.char_offsets[ch]
        return (self.font.font_11x44[o:o + 11], 1)


    def bitmap_text(self, text):
        """Returns a tuple of (buffer, length_in_byte_columns_aka_chars)
          We preprocess the text string for substitution patterns
          "::" is replaced with a single ":"
          ":1: is replaced with CTRL-A referencing the first preloaded or loaded image.
          ":happy:" is replaced with a reference to a builtin smiley glyph
          ":heart:" is replaced with a reference to a builtin heart glyph
          ":gfx/logo.png:" preloads the file gfx/logo.png and is replaced the corresponding control char.
        """

        def replace_symbolic(m):
            name = m.group(1)
            if name == '':
                return ':'
            if re.match('^[0-9]*$', name):  # py3 name.isdecimal()
                return chr(int(name))
            if '.' in name:
                self.bitmap_preloaded.append(SimpleTextAndIcons.bitmap_img(name))
                return chr(len(self.bitmap_preloaded) - 1)
            return SimpleTextAndIcons.bitmap_named[name][2]

        text = re.sub(r':([^:]*):', replace_symbolic, text)
        buf = array('B')
        cols = 0
        for c in text:
            (b, n) = self.bitmap_char(c)
            buf.extend(b)
            cols += n
        return (buf, cols)


    @staticmethod
    def bitmap_img(file):
        """Returns a tuple of (buffer, length_in_byte_columns) representing the given image file.
            It has to be an 8-bit grayscale image or a color image with 8 bit per channel. Color pixels are converted to
            grayscale by arithmetic mean. Threshold for an active led is then > 127.
            If the width is not a multiple on 8 it will be padded with empty pixel-columns.
        """
        from PIL import Image

        im = Image.open(file)
        print("fetching bitmap from file %s -> (%d x %d)" % (file, im.width, im.height))
        if im.height != 11:
            sys.exit("%s: image height must be 11px. Seen %d" % (file, im.height))
        buf = array('B')
        cols = int((im.width + 7) / 8)
        for col in range(cols):
            for row in range(11):  # [0..10]
                byte_val = 0
                for bit in range(8):  # [0..7]
                    bit_val = 0
                    x = 8 * col + bit
                    if x < im.width and row < im.height:
                        pixel_color = im.getpixel((x, row))
                        if isinstance(pixel_color, tuple):
                            monochrome_color = sum(pixel_color[:3]) / len(pixel_color[:3])
                        elif isinstance(pixel_color, int):
                            monochrome_color = pixel_color
                        else:
                            sys.exit("%s: Unknown pixel format detected (%s)!" % (file, pixel_color))
                        if monochrome_color > 127:
                            bit_val = 1 << (7 - bit)
                        byte_val += bit_val
                buf.append(byte_val)
        im.close()
        return (buf, cols)


    def bitmap(self, arg):
        """If arg is a valid and existing path name, we load it as an image.
            Otherwise, we take it as a string (with ":"-notation, see bitmap_text()).
        """
        if os.path.exists(arg):
            return SimpleTextAndIcons.bitmap_img(arg)
        return self.bitmap_text(arg)


class LedNameBadge:
    _protocol_header_template = (
        0x77, 0x61, 0x6e, 0x67, 0x00, 0x00, 0x00, 0x00, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, # [5]==0x49 tycks ev inte visa M1-8
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    )
    _have_pyhidapi = False

    try:
        if sys.version_info[0] < 3:
            print("Preferring Pyusb over Pyhidapi with Python 2.x")
            raise Exception("Prefer usb.core with python-2.x because of https://github.com/jnweiger/led-badge-ls32/issues/9")
        import pyhidapi
        pyhidapi.hid_init()
        _have_pyhidapi = True
        print("Pyhidapi detected")
    except:
        try:
            import usb.core
            print("Pyusb detected")
        except:
            try:
                import bleak
                print("bleak detected")
            except:
                print("ERROR: This script needs either one of the modules 'pyhidapi', 'pyusb' or 'bleak' to function.")
                if sys.platform == "darwin":
                    print("""Please install the dependencies.

    For uploading via USB:
        1) Install the python module 'pyhidapi'
            $ python3 -m pip install pyhidapi
        2) Install the library 'libhidapi'
            $ brew install hidapi

    For uploading via Bluetooth LE:
        1) Install the python module 'bleak'
            $ python3 -m pip install bleak
                    """)
                elif sys.platform == "linux":
                    print("""Please install the dependencies according to your distro, architecture and lib preference.

    For uploading via USB using libhidapi:
        1) Install the python module 'pyhidapi'
            All distros:            $ python3 -m pip install pyhidapi
        2) Install the library 'libhidapi'
            Debian/Ubuntu:          $ sudo apt-get install libhidapi-hidraw0
            Arch:                   $ sudo pacman -S hidapi
        3) Link the library so that pyhidapi can find it
            Debian/Ubuntu X86:      $ sudo ln -s /usr/lib/x86_64-linux-gnu/libhidapi-hidraw.so.0 /usr/local/lib/libhidapi-hidraw.so.0
            Debian/Ubuntu aarch64:  $ sudo ln -s /usr/lib/aarch64-linux-gnu/libhidapi-hidraw.so.0 /usr/local/lib/libhidapi-hidraw.so.0
            Arch:                   $ sudo ln -s /usr/lib/libhidapi-libusb.so.0 /usr/local/lib/libhidapi-libusb.so.0
        4) [Debian/Ubuntu]: Allow the script access to the USB device when run by a non-root user
            a) $ sudo cp 69-led-badge-44x11.rules /lib/udev/rules.d/
            b) Unplug and re-connect the badge

    For uploading via USB using pyusb:
        1) Install the pyUSB package
            Debian:             $ sudo apt-get install python3-usb
            Arch (not tested):  $ sudo pacman -S python-pyusb

    For uploading via Bluetooth LE:
        1) Install the python module 'bleak'
            All distros:        $ python3 -m pip install bleak
                    """)
                else: # windows?
                    print("""Please try with Linux or MacOS or help us implement support for """ + sys.platform)
                sys.exit(1)


    @staticmethod
    def _prepare_iterable(iterable, min_, max_):
        try:
            iterable = [min(max(x, min_), max_) for x in iterable]
            iterable = tuple(iterable) + (iterable[-1],) * (8 - len(iterable))  # repeat last element
            return iterable
        except:
            raise TypeError("Please give a list or tuple with at least one number: " + str(iterable))


    @staticmethod
    def header(lengths, speeds, modes, blinks, ants, brightness=100, date=datetime.now()):
        """Create a protocol header
            * length, speeds, modes, blinks, ants are iterables with at least one element
            * lengths[0] is the number of chars/byte-columns of the first text/bitmap, lengths[1] of the second,
              and so on...
            * len(length) should match the designated bitmap data
            * speeds come in as 1..8, but will be decremented to 0..7, here.
            * modes: 0..8
            * blinks and ants: 0..1 or even False..True,
            * brightness, if given, is any number, but it'll be limited to 25, 50, 75, 100 (percent), here
            * date, if given, is a datetime object. It will be written in the header, but is not to be seen on the
              devices screen.
        """
        try:
            lengths_sum = sum(lengths)
        except:
            raise TypeError("Please give a list or tuple with at least one number: " + str(lengths))
        if lengths_sum > (8192 - len(LedNameBadge._protocol_header_template)) / 11 + 1:
            raise ValueError("The given lengths seem to be far too high: " + str(lengths))


        ants = LedNameBadge._prepare_iterable(ants, 0, 1)
        blinks = LedNameBadge._prepare_iterable(blinks, 0, 1)
        speeds = LedNameBadge._prepare_iterable(speeds, 0, 8)
        modes = LedNameBadge._prepare_iterable(modes, 0, 9) # 0, 8

        speeds = [x - 1 for x in speeds]

        h = list(LedNameBadge._protocol_header_template)

        if brightness <= 25:
            h[5] = 0x39
        elif brightness <= 50:
            h[5] = 0x19
        elif brightness <= 75:
            h[5] = 0x09
        else:
            h[5] = 0x01

        for i in range(8):
            h[6] += blinks[i] << i
            h[7] += ants[i] << i

        for i in range(8):
            h[8 + i] = 16 * speeds[i] + modes[i]

        for i in range(len(lengths)):
            h[17 + (2 * i) - 1] = lengths[i] // 256
            h[17 + (2 * i)] = lengths[i] % 256

        try:
            h[38 + 0] = date.year % 100
            h[38 + 1] = date.month
            h[38 + 2] = date.day
            h[38 + 3] = date.hour
            h[38 + 4] = date.minute
            h[38 + 5] = date.second
        except:
            raise TypeError("Please give a datetime object: " + str(date))

        return h

    @staticmethod
    def write(buf):
        """Write the given buffer to the device.
            It has to begin with a protocol header as provided by header() and followed by the bitmap data.
            In short: the bitmap data is organized in bytes with 8 horizontal pixels per byte and 11 resp. 12
            bytes per (8 pixels wide) byte-column. Then just put one byte-column after the other and one bitmap
            after the other.
        """
        need_padding = len(buf) % 64
        if need_padding:
            buf.extend((0,) * (64 - need_padding))

        if len(buf) > 8192:
            print("Writing more than 8192 bytes damages the display!")
            sys.exit(1)

        if LedNameBadge._have_pyhidapi:
            dev_info = LedNameBadge.pyhidapi.hid_enumerate(0x0416, 0x5020)
            # dev = pyhidapi.hid_open(0x0416, 0x5020)
            if dev_info:
                dev = LedNameBadge.pyhidapi.hid_open_path(dev_info[0].path)
                print("using [%s %s] int=%d page=%s via pyHIDAPI" % (
                    dev_info[0].manufacturer_string, dev_info[0].product_string, dev_info[0].interface_number, dev_info[0].usage_page))
            else:
                print("No led tag with vendorID 0x0416 and productID 0x5020 found.")
                print("Connect the led tag and run this tool as root.")
                sys.exit(1)
            for i in range(int(len(buf)/64)):
                # sendbuf must contain "report ID" as first byte. "0" does the job here.
                sendbuf=array('B',[0])
                # Then, put the 64 payload bytes into the buffer
                sendbuf.extend(buf[i*64:i*64+64])
                LedNameBadge.pyhidapi.hid_write(dev, sendbuf)
            LedNameBadge.pyhidapi.hid_close(dev)
        else:
            dev = LedNameBadge.usb.core.find(idVendor=0x0416, idProduct=0x5020)
            if dev is None:
                print("No led tag with vendorID 0x0416 and productID 0x5020 found.")
                print("Connect the led tag and run this tool as root.")
                sys.exit(1)
            try:
                # win32: NotImplementedError: is_kernel_driver_active
                if dev.is_kernel_driver_active(0):
                    dev.detach_kernel_driver(0)
            except:
                pass
            dev.set_configuration()
            print("using [%s %s] bus=%d dev=%d" % (dev.manufacturer, dev.product, dev.bus, dev.address))
            endpoint = 1
            i = 0
            while i < int(len(buf) / 64):
                time.sleep(0.1)
                try:
                    dev.write(endpoint, buf[i * 64:i * 64 + 64])
                    i += 1
                except ValueError:
                    if endpoint == 1:
                        endpoint = 2
                        i = 0

    @staticmethod
    def write_ble(buf):
        """Write the given buffer to the device using bluetooth low-energy.
            It has to begin with a protocol header as provided by header() and followed by the bitmap data.
            In short: the bitmap data is organized in bytes with 8 horizontal pixels per byte and 11 resp. 12
            bytes per (8 pixels wide) byte-column. Then just put one byte-column after the other and one bitmap
            after the other.
        """
        try:
            import asyncio
            import platform
            from bleak import BleakScanner, BleakClient
        except:
            print("ERROR: Need the bleak module for bluetooth support - Please install it with:")
            print("pip3 install bleak")
            print("- or -")
            print("python3 -m pip install bleak")
            sys.exit()

        is_macos = True if platform.system() == "Darwin" else False

        async def get_badge():
            device = await BleakScanner.find_device_by_name("LSLED", cb=dict(use_bdaddr=not is_macos))
            if device:
                address = device.address
                print("Found 'LSLED' at address " + str(address))
            if device is None:
                sys.exit("Could not find a device with the name 'LSLED'")
            
            print("Looking for characteristic FEE1 of service FEE0")
            async with BleakClient(device, services=['fee0'],) as client:
                for service in client.services:
                    for char in service.characteristics:
                        char_fee1 = char.uuid
            return [address, char_fee1]
        try:
            badge = asyncio.run(get_badge())
            address = badge[0]
            char_fee1 = badge[1]
        except Exception as e:
            sys.exit(f"ERROR: Could not find service or charasteristic:\n{str(e)}")
        
        buf_list = buf.tolist()
        def split_list(buf_list, chunk_size):
            return [buf_list[i:i + chunk_size] for i in range(0, len(buf_list), chunk_size)]
        chunks = split_list(buf_list, 16)
        write_requests = []
        for chunk in chunks:
            chunk_string = ""
            for int_byte in chunk:
                hex_byte = f'{int_byte:x}'.zfill(2).upper()
                chunk_string += hex_byte
            write_requests.append(chunk_string)
        async def go():
            async with BleakClient(address) as client:
                if client.is_connected: print("Connection established - Sending message") 
                for i in range (0, len(write_requests)):
                    byte_array = bytes.fromhex(write_requests[i])
                    await client.write_gatt_char(char_fee1, byte_array, response=True)
                    #print(f"Written byte array: {str(byte_array)}")
        asyncio.run(go())
        print("Done")


def split_to_ints(list_str):
    return [int(x) for x in re.split(r'[\s,]+', list_str)]

def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description='Upload messages or graphics to a 11x44 led badge via USB HID.\nVersion %s from https://github.com/jnweiger/led-badge-ls32\n -- see there for more examples and for updates.' % __version,
                                     epilog='Example combining image and text:\n sudo %s "I:HEART2:you"' % sys.argv[0])
    parser.add_argument('-t', '--type', default='11x44',
                        help="Type of display: supported values are 12x48 or (default) 11x44. Rename the program to led-badge-12x48, to switch the default.")
    parser.add_argument('-H', '--hid', default='0', help="Set to 1 to ensure connect via HID API, program will then not fallback to usb.core library")
    parser.add_argument('-w', '--ble', action="store_true", help="Send message over low-energy bluetooth instead of USB. Dependent of bleak module.")
    parser.add_argument('-s', '--speed', default='4', help="Scroll speed (Range 1..8). Up to 8 comma-separated values")
    parser.add_argument('-B', '--brightness', default='100',
                        help="Brightness for the display in percent: 25, 50, 75, or 100")
    parser.add_argument('-m', '--mode', default='0',
                        help="Up to 8 mode values: Scroll-left(0) -right(1) -up(2) -down(3); still-centered(4); animation(5); drop-down(6); curtain(7); laser(8); See '--mode-help' for more details.")
    parser.add_argument('-b', '--blink', default='0', help="1: blinking, 0: normal. Up to 8 comma-separated values")
    parser.add_argument('-a', '--ants', default='0', help="1: animated border, 0: normal. Up to 8 comma-separated values")
    parser.add_argument('-f', '--font', default='default', help="default: default font. Custom fonts are placed in fonts/ and used here.")
    parser.add_argument('-p', '--preload', metavar='FILE', action='append',
                        help=argparse.SUPPRESS)  # "Load bitmap images. Use ^A, ^B, ^C, ... in text messages to make them visible. Deprecated, embed within ':' instead")
    parser.add_argument('-l', '--list-names', action='version', help="list named icons to be embedded in messages and exit",
                        version=':' + ':  :'.join(SimpleTextAndIcons._get_named_bitmaps_keys()) + ':  ::  or e.g. :path/to/some_icon.png:')
    parser.add_argument('message', metavar='MESSAGE', nargs='+',
                        help="Up to 8 message texts with embedded builtin icons or loaded images within colons(:) -- See -l for a list of builtins")
    parser.add_argument('--mode-help', action='version', help=argparse.SUPPRESS, version="""
    
    -m 5 "Animation"
    
     Animation frames are 6 character (or 48px) wide. Upload an animation of
     N frames as one image N*48 pixels wide, 11 pixels high.
     Frames run from left to right and repeat endless.
     Speeds [1..8] result in ca. [1.2 1.3 2.0 2.4 2.8 4.5 7.5 15] fps.
    
     Example of a slowly beating heart:
      sudo %s -s1 -m5 "  :heart2:    :HEART2:"
    
    -m 9 "Smooth"
    -m 10 "Rotate"
    
     These modes are mentioned in the BMP Badge software.
     Text is shown static, or sometimes (longer texts?) not shown at all.
     One significant difference is: The text of the first message stays visible after
     upload, even if the USB cable remains connected.
     (No "rotation" or "smoothing"(?) effect can be expected, though)
    """ % sys.argv[0])
    args = parser.parse_args()

    fontname = args.font
    font = getattr(importlib.import_module("fonts."+fontname), 'Font')

    creator = SimpleTextAndIcons(font)

    if args.preload:
        for filename in args.preload:
            creator.add_preload_img(filename)

    msg_bitmaps = []
    for msg_arg in args.message:
        msg_bitmaps.append(creator.bitmap(msg_arg))

    if creator.are_preloaded_unused():
        print(
            "\nWARNING:\n Your preloaded images are not used.\n Try without '-p' or embed the control character '^A' in your message.\n")

    if '12' in args.type or '12' in sys.argv[0]:
        print("Type: 12x48")
        for msg_bitmap in msg_bitmaps:
            # trivial hack to support 12x48 badges:
            # patch extra empty lines into the message stream.
            for i in reversed(range(1, int(len(msg_bitmap[0]) / 11) + 1)):
                msg_bitmap[0][i * 11:i * 11] = array('B', [0])
    else:
        print("Type: 11x44")

    lengths = [b[1] for b in msg_bitmaps]
    speeds = split_to_ints(args.speed)
    modes = split_to_ints(args.mode)
    blinks = split_to_ints(args.blink)
    ants = split_to_ints(args.ants)
    brightness = int(args.brightness)
    use_ble = args.ble
    buf = array('B')
    buf.extend(LedNameBadge.header(lengths, speeds, modes, blinks, ants, brightness))

    for msg_bitmap in msg_bitmaps:
        buf.extend(msg_bitmap[0])

    if not LedNameBadge._have_pyhidapi:
        if args.hid != "0":
            sys.exit("HID API access is needed but not initialized. Fix your setup")
   
    if use_ble:
        LedNameBadge.write_ble(buf)
    else:
        LedNameBadge.write(buf)


if __name__ == '__main__':
    main()
