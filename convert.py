#!/usr/bin/env python3
import sys

if (len(sys.argv) == 1):
    res = ""
    with open ("letter.txt", "r") as f:
        lines = f.readlines()
    for line in lines:
        #print(hex(bin(int(line.rstrip()))))
        intt = int(line.rstrip(), 2)
        res += hex(intt) + ", "
        #print(f"{hex(intt):#0{4}x}")
    print(res)
else:
    lista = sys.argv[1]
    listb = lista.split(", ")
    for i in listb:
        print(bin(int(i, 16))[2:].zfill(8))
